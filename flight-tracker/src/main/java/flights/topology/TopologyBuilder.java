package flights.topology;

import flights.serde.Serde;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import radar.AirportUpdateEvent;
import radar.Flight;
import radar.FlightUpdateEvent;

public class TopologyBuilder implements Serde {

    private Properties config;

    public TopologyBuilder(Properties properties) {
        this.config = properties;
    }

    private static final Logger logger = LogManager.getLogger(TopologyBuilder.class);

    public Topology build() {
        StreamsBuilder builder = new StreamsBuilder();
        String schemaRegistry = config.getProperty("kafka.schema.registry.url");

        KStream<String, FlightUpdateEvent> flightInputStream = builder.stream(
                config.getProperty("kafka.topic.flight.update.events"),
                Consumed.with(Serde.stringSerde, Serde.specificSerde(FlightUpdateEvent.class, schemaRegistry)));

        KStream<String, Flight> flightOutputStream = flightInputStream.map((key, value) -> {
            Flight flight = new Flight();
            flight.setId(value.getId());
            flight.setTo(value.getDestination().toString().split("->")[0]);
            flight.setFrom(value.getDestination().toString().split("->")[1]);
            flight.setDepartureTimestamp(value.getSTD());
            flight.setArrivalTimestamp(value.getSTA());
            flight.setDuration((value.getSTA() - value.getSTD()) / 1000 / 60); // convert to minutes
            //flight.setDepartureDatetime(convertToISO8601(value.getSTD(), value.getTimezones().split("-")[0]));
            //flight.setArrivalDatetime(convertToISO8601(value.getSTA(), value.getTimezones().split("-")[1]));
            flight.setDepartureAirportCode(value.getDestination().toString().split("-")[0]);
            flight.setArrivalAirportCode(value.getDestination().toString().split("-")[1]);
//            System.out.println(flight);
            return new KeyValue<>(key, flight);
        });
        flightOutputStream.to("radar.flights", Produced.with(Serde.stringSerde, Serde.specificSerde(Flight.class, schemaRegistry)));
//        flightOutputStream
//                .foreach((k,v)->{
//            System.out.println(v);
//        });

//        flightInputStream
//                .foreach((k, v) -> {
//                    // print the record to the console
//                    System.out.println("Key: " + k + " Value: " + v);
//                });
        GlobalKTable<String, AirportUpdateEvent> airportTable = builder.globalTable(
                config.getProperty("kafka.topic.airport.update.events"),
                Consumed.with(Serde.stringSerde, Serde.specificSerde(AirportUpdateEvent.class, schemaRegistry)));

        return builder.build();
    }
}
